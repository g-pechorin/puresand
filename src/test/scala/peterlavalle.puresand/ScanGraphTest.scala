package peterlavalle.puresand

import org.scalatest.funsuite.AnyFunSuite

class ScanGraphTest extends AnyFunSuite {
	test("hello world") {
		val actual: Set[String] = ScanGraph.map(_._1).toSet
		val expected = Set(
			"peterlavalle.puresand/FlipBurger",
			"peterlavalle.puresand/page/Fogurt"
		)

		implicit class ExtendSet[T](iterable: Iterable[T]) {
			@inline
			def containsAll(them: Iterable[T]): Boolean =
				them.filterNot(iterable.toSet).isEmpty
		}

		assert(
			actual containsAll expected
		)
	}
}
