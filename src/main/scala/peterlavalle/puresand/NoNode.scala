package peterlavalle.puresand

import java.io._
import java.util

import scala.language.postfixOps
import scala.sys.process.{ProcessLogger, _}

trait NoNode {

	def apply(cmd: String, args: String*): Command

	trait Command extends (File => ProcessLogger => Int) {
		def ! : (Int, List[String]) = this ! null

		def !(cwd: File = null): (Int, List[String]) = {
			val lines = new util.LinkedList[String]()
			apply(
				cwd
			)(
				ProcessLogger(
					(o: String) => lines.add(';' + o),
					(e: String) => lines.add('!' + e)
				)
			) -> {
				import scala.collection.JavaConverters._
				lines.asScala.toList
			}
		}
	}
}

object NoNode {
	def apply(dump: File): NoNode =
		if (dump != dump.getAbsoluteFile)
			apply(dump.getAbsoluteFile)
		else
			new NoNode {

				// setup the install
				lazy val spago: File = Extract(dump)

				override def apply(cmd: String, args: String*): Command = {
					(cwd: File) =>
						val sysPath: String = System.getenv("PATH")
						val newPath: String = spago.getParentFile.getAbsolutePath + File.pathSeparator + sysPath

						val process: ProcessBuilder =
							Process(
								// build the spago command
								command = spago.getAbsolutePath :: cmd :: args.toList,

								// workout where to run
								cwd =
									if (null == cwd)
										dump
									else
										cwd.getAbsoluteFile,

								// prepend the extacted things to the PATH
								"PATH" -> newPath
							)


						(log: ProcessLogger) =>
							process ! log
				}
			}

}
