package peterlavalle.puresand

import io.github.classgraph.{ClassGraph, Resource, ResourceList}

import scala.collection.convert.ImplicitConversions.{`list asScalaBuffer`, `map AsScala`}
import scala.io.Source

object ScanGraph extends Iterable[(String, Either[Resource, (Resource, Resource)])] {

	private def scan: Stream[(String, Either[Resource, (Resource, Resource)])] = {

		def singleIdenticalResource(ext: String): Stream[(String, Resource)] = {

			def singleIdenticalResource(list: ResourceList): Resource = {
				if (0 == list.size())
					sys.error("need a resource - at least one")
				else if (1 == list.size())
					list.get(0)
				else {
					// extract all the content
					val s: List[String] =
						list.toList.map {
							resource =>
								val src = Source.fromInputStream(resource.open())
								val str = src.mkString
								src.close()
								str
						}

					// compare the content
					if (!s.tail.forall(_ == s.head))
						sys.error("multple differend ones;" + ext)
					else
						list.head
				}
			}

			new ClassGraph()
				.scan().getResourcesWithExtension(ext)
				.asMap().toStream
				.map {
					case (k, v) =>
						val key: String = k.reverse.drop(ext.length).reverse
						key -> singleIdenticalResource(v)
				}
		}

		val purs: Stream[(String, Resource)] = singleIdenticalResource(".purs")
		val ecma: Stream[(String, Resource)] = singleIdenticalResource(".js")

		purs
			.map {
				case (name, purs) =>
					require(null != name, "name is null?")
					require(null != purs, "purs is null?")
					ecma.find {
						case null =>
							sys.error("okay ... what?")
						case each: (String, Resource) =>

							require(null != each, "how'd that happen?")

							require(null != each._1, "okay ... null?")


							each._1 == name
					}
						.map {
							case (_, ecma) =>
								require(null != ecma)
								name -> Right((purs, ecma))
						}
						.getOrElse(name -> Left(purs))
			}
	}

	override def iterator: Iterator[(String, Either[Resource, (Resource, Resource)])] =
		scan.iterator
}
