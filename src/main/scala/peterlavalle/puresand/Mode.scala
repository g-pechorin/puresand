package peterlavalle.puresand

import java.io.File

import scala.language.postfixOps

trait Mode {
	def |(mode: Mode): Mode =
		(file: File) => {
			apply(file)
			mode(file)
		}

	def flag: Int = sys.error("no flag here")

	def apply(file: File): Unit
}

object Mode {


	def apply(mode: Int): Mode =
		List(Read, Write, Execute)
			.filterNot(m => 0 == (mode & m.flag))
			.reduce((_: Mode) | (_: Mode))

	case object Read extends Mode {
		override def apply(file: File): Unit = if (!file.canRead) require(file.setReadable(true))

		override def flag: Int = 4
	}

	case object Write extends Mode {
		override def apply(file: File): Unit = if (!file.canWrite) require(file.setWritable(true))

		override def flag: Int = 2
	}

	case object Execute extends Mode {
		override def apply(file: File): Unit = if (!file.canExecute) require(file.setExecutable(true))

		override def flag: Int = 1
	}
}
